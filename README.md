# evaneos-refactoring-kata

Replace placeholders in texts

**Evaneos** is present on a lot of countries and we have some message templates we want to send
in different languages. To do that, we've developed `TemplateManager` whose job is to replace
placeholders in texts by travel related information.

## Exemple
```php
$faker = \Faker\Factory::create();

$template = new Template(
    1,
    'Votre voyage avec une agence locale [quote:destination_name]',
    "
Bonjour [user:first_name], [quote:summary_html], [quote:summary], [quote:destination_link]

Merci d'avoir contacté un agent local pour votre voyage [quote:destination_name].

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
");
$templateManager = new TemplateManager();

$message = $templateManager->getTemplateComputed(
    $template,
    [
        'quote' => new Quote($faker->randomNumber(), $faker->randomNumber(), $faker->randomNumber(), $faker->date())
    ]
);
```

## Result
```
Votre voyage avec une agence locale Lithuania

Bonjour Ruth, <p>909789</p>, 909789, http://mclaughlin.com/dolorum-in-quo-magni-aut-illo-magnam/Lithuania/quote/909789

Merci d'avoir contacté un agent local pour votre voyage Lithuania.

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com

```

## License

 © [Evaneos](evaneos.com)