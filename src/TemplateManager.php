<?php

class TemplateManager
{
    /**
     * Replace all the placeholder for a Template (subject & content)
     *
     * @param Template $tpl
     * @param array $data
     *
     * @return Template
     */
    public function getTemplateComputed(Template $tpl, array $data)
    {
        if (!$tpl) {
            throw new \RuntimeException('no tpl given');
        }

        $replaced = clone($tpl);
        $replaced->setSubject($this->computeText($tpl->getSubject(), $data));
        $replaced->setContent($this->computeText($tpl->getContent(), $data));

        return $replaced;
    }

    /**
     * Get all placeholder values and replace it in a text
     *
     * @param string $text
     * @param array $data
     *
     * @return string string
     */
    private function computeText($text, array $data)
    {
        $quote = (isset($data['quote']) && $data['quote'] instanceof Quote) ? $data['quote'] : null;
        $quoteParams = ($quote) ? $this->defineQuoteParamsValues($quote, $text) : [];

        $user = (isset($data['user']) && ($data['user'] instanceof User)) ? $data['user'] : ApplicationContext::getInstance()->getCurrentUser();
        $userParams = ($user) ? $this->defineUserParamsValues($user, $text) : [];

        $paramsToReplace = array_merge($userParams, $quoteParams);

        return $this->replacePlaceholder($text, $paramsToReplace);
    }

    /**
     * Replace all the placeholder in a text
     *
     * @param string $text
     * @param array $params
     *
     * @return string $text
     */
    private function replacePlaceholder($text, $params)
    {
        foreach ($params as $key => $value) {
            $text = str_replace($key, $value, $text);
        }

        return $text;
    }

    /**
     * This function generate all [quote:*] placeholder values
     *
     * @param Quote $quote
     * @param string $text
     *
     * @return array $availableQuoteParams
     */
    private function defineQuoteParamsValues(Quote $quote, $text)
    {
        $availableQuoteParams = array(
            '[quote:destination_link]' => null,
            '[quote:summary_html]' => null,
            '[quote:summary]' => null,
            '[quote:destination_name]' => null
        );

        $site = SiteRepository::getInstance()->getById($quote->getSiteId());
        $destination = DestinationRepository::getInstance()->getById($quote->getDestinationId());

        if (strpos($text, '[quote:destination_link]')) {
            $destination = DestinationRepository::getInstance()->getById($quote->getDestinationId());
            $availableQuoteParams['[quote:destination_link]'] = (isset($destination)) ? $site->getUrl() . '/' . $destination->getCountryName() . '/quote/' . $quote->getId() : null;
        }

        if (strpos($text, '[quote:summary_html]')) {
            $availableQuoteParams['[quote:summary_html]'] = $quote->renderHtml();
        }

        if (strpos($text, '[quote:summary]')) {
            $availableQuoteParams['[quote:summary]'] = $quote->renderText();
        }

        if (strpos($text, '[quote:destination_name]')) {
            $availableQuoteParams['[quote:destination_name]'] = $destination->getCountryName();
        }

        return $availableQuoteParams;
    }

    /**
     * This function generate all [user:*] placeholder values
     *
     * @param User $user
     * @param string $text
     *
     * @return array $availableUserParams
     */
    private function defineUserParamsValues(User $user, $text)
    {
        $availableUserParams = array(
            '[user:first_name]' => null,
        );

        if (strpos($text, '[user:first_name]')) {
            $availableUserParams['[user:first_name]'] = ucfirst(mb_strtolower($user->getFirstName()));
        }

        return $availableUserParams;
    }
}
