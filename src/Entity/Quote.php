<?php

class Quote
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $siteId;

    /**
     * @var integer
     */
    private $destinationId;

    /**
     * @var string
     */
    private $dateQuoted;

    public function __construct($id, $siteId, $destinationId, $dateQuoted)
    {
        $this->setId($id);
        $this->setSiteId($siteId);
        $this->setDestinationId($destinationId);
        $this->setDateQuoted($dateQuoted);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getSiteId()
    {
        return $this->siteId;
    }

    /**
     * @param int $siteId
     */
    public function setSiteId($siteId)
    {
        $this->siteId = $siteId;
    }

    /**
     * @return int
     */
    public function getDestinationId()
    {
        return $this->destinationId;
    }

    /**
     * @param int $destinationId
     */
    public function setDestinationId($destinationId)
    {
        $this->destinationId = $destinationId;
    }

    /**
     * @return string
     */
    public function getDateQuoted()
    {
        return $this->dateQuoted;
    }

    /**
     * @param string $dateQuoted
     */
    public function setDateQuoted($dateQuoted)
    {
        $this->dateQuoted = $dateQuoted;
    }

    /**
     * @return string
     */
    public function renderHtml()
    {
        return '<p>' . $this->id . '</p>';
    }

    /**
     * @return string
     */
    public function renderText()
    {
        return (string) $this->id;
    }
}