<?php

require_once __DIR__ . '/../src/Entity/Destination.php';
require_once __DIR__ . '/../src/Entity/Quote.php';
require_once __DIR__ . '/../src/Entity/Site.php';
require_once __DIR__ . '/../src/Entity/Template.php';
require_once __DIR__ . '/../src/Entity/User.php';
require_once __DIR__ . '/../src/Helper/SingletonTrait.php';
require_once __DIR__ . '/../src/Context/ApplicationContext.php';
require_once __DIR__ . '/../src/Repository/Repository.php';
require_once __DIR__ . '/../src/Repository/DestinationRepository.php';
require_once __DIR__ . '/../src/Repository/QuoteRepository.php';
require_once __DIR__ . '/../src/Repository/SiteRepository.php';
require_once __DIR__ . '/../src/TemplateManager.php';

class TemplateManagerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var TemplateManager
     */
    private $templateManager;

    /**
     * @var \Faker\Factory
     */
    private $faker;

    /**
     * Init the mocks
     */
    public function setUp()
    {
        $this->templateManager = new TemplateManager();
        $this->faker = \Faker\Factory::create();
    }

    /**
     * @test
     */
    public function testGetTemplateComputed()
    {
        $expectedDestination = DestinationRepository::getInstance()->getById($this->faker->randomNumber());
        $expectedUser = ApplicationContext::getInstance()->getCurrentUser();

        $quote = new Quote($this->faker->randomNumber(), $this->faker->randomNumber(), $this->faker->randomNumber(), $this->faker->date());

        $template = new Template(
            1,
            'Votre voyage avec une agence locale [quote:destination_name]',
            "
Bonjour [user:first_name],

Merci d'avoir contacté un agent local pour votre voyage [quote:destination_name].

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
");
        $message = $this->templateManager->getTemplateComputed(
            $template,
            [
                'quote' => $quote
            ]
        );

        $this->assertEquals('Votre voyage avec une agence locale ' . $expectedDestination->getCountryName(), $message->getSubject());
        $this->assertEquals("
Bonjour " . $expectedUser->getFirstName() . ",

Merci d'avoir contacté un agent local pour votre voyage " . $expectedDestination->getCountryName() . ".

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
", $message->getContent());
    }

    /**
     * @throws ReflectionException
     */
    public function testDefineUserParamsValuesWithGoodText()
    {
        $user = new User(1, 'test-first-name', 'test-last-name', 'test-email');
        $text = "Hello [user:first_name],";

        $reflector = new ReflectionClass('TemplateManager');
        $method = $reflector->getMethod('defineUserParamsValues');
        $method->setAccessible(true);

        $result = $method->invokeArgs($this->templateManager, array($user, $text));

        $this->assertEquals('Test-first-name', $result['[user:first_name]']);
    }

    /**
     * @throws ReflectionException
     */
    public function testDefineUserParamsValuesWithWrongText()
    {
        $user = new User(1, 'test-first-name', 'test-last-name', 'test-email');
        $text = "Hello [user:foo],";

        $reflector = new ReflectionClass('TemplateManager');
        $method = $reflector->getMethod('defineUserParamsValues');
        $method->setAccessible(true);

        $result = $method->invokeArgs($this->templateManager, array($user, $text));

        $this->assertEquals(null, $result['[user:first_name]']);
    }
}
